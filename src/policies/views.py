from django.shortcuts import render

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from .models import Policy
from .serializers import PolicySerializer

# Create your views here.

class PolicyAPI(APIView):
    """
    Called when a policy is uploaded.
    """
    def get(self, request, format=None):
        all_policies = Policy.objects.all()
        serializer = PolicySerializer(all_policies, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = PolicySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
