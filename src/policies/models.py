from django.db import models

# Create your models here.

class Benefit(models.Model):
    """Could be a choice field in the policy database,
       but that would require re-making migrations each
       time we want to add a benefit."""
    pass

class Currency(models.Model):
    """Could be a choice field in the policy database,
       but that would require re-making migrations each
       time we want to add a currency."""
    pass

class Policy(models.Model):
    external_user_id = models.CharField(max_length=255)
    benefit = models.CharField(max_length=255)
    currency = models.CharField(max_length=255)
    total_max_amount = models.BigIntegerField()

    class Meta:
       unique_together = ("external_user_id", "benefit", "currency")
