"""insurer_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url  # noqa

from payments import views as payments_view
from policies import views as policies_view


urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^payment', payments_view.PaymentAPI.as_view(), name='payment'),
    url(r'^policy', policies_view.PolicyAPI.as_view(), name='policy'),
]
