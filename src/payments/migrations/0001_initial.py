# Generated by Django 3.0.2 on 2020-01-15 12:07

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('external_user_id', models.CharField(max_length=255)),
                ('benefit', models.CharField(max_length=255)),
                ('currency', models.CharField(max_length=255)),
                ('amount', models.BigIntegerField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
