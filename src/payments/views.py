from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.db.models import Sum
from django.db.models.functions import Coalesce

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.parsers import JSONParser

from .serializers import PaymentSerializer
from policies.models import Policy
from .models import Payment

# Create your views here.

# Authorization reasons
# If no policy for given external_user_id, benefit and currency exists
POLICY_NOT_FOUND = "POLICY_NOT_FOUND"
# If the total amount of this and all others accepted authorizations for the matched policy exceed the total_max_amount of the policy
POLICY_AMOUNT_EXCEEDED = "POLICY_AMOUNT_EXCEEDED"

def isAuthorized(serializer):
    """
    Verifies if a payment should be authorized.
    If the policy for given external_user_id,
    benefit and currency exists, and the amount
    of this authorization + all authorized payments
    for this policy does not exceed the policy's
    total_max_amount, the authorization should be authorized.

    input: serialized payment information
    output: Tuple of: Boolean(True/False) indicating authorization
            of payment and reason(POLICY_NOT_FOUND, POLICY_AMOUNT_EXCEEDED)
            for possible refusal of authorization.
    """
    serializer_valid_data = PaymentSerializer(serializer).data
    sum_of_all_payments = Payment.objects.filter(external_user_id=serializer_valid_data['external_user_id'],
    benefit=serializer_valid_data['benefit'], currency=serializer_valid_data['currency']).aggregate(total_amount=Coalesce(Sum('amount'), 0))

    policy = Policy.objects.filter(external_user_id=serializer_valid_data['external_user_id'],
    benefit=serializer_valid_data['benefit'], currency=serializer_valid_data['currency'])

    policy_total_amount = policy.aggregate(total_amount=Coalesce(Sum('total_max_amount'), 0))

    reason = None
    sum_of_payments_gt_policy_total_amount = (sum_of_all_payments['total_amount'] + serializer_valid_data['amount']) > policy_total_amount['total_amount']
    if not policy.exists():
        reason = POLICY_NOT_FOUND
    else:
        if sum_of_payments_gt_policy_total_amount:
            reason = POLICY_AMOUNT_EXCEEDED
    return (policy.exists() and (not sum_of_payments_gt_policy_total_amount), reason)


class PaymentAPI(APIView):
    """
    Called when a customer performs a payment.
    """
    def get(self, request, format=None):
        payments = Payment.objects.all()
        serializer = PaymentSerializer(payments, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = PaymentSerializer(data=request.data)
        authorization = (False, None)
        if serializer.is_valid():
            authorization = isAuthorized(serializer.validated_data)
        is_authorized = authorization[0]
        reason = authorization[1]
        if serializer.is_valid() and is_authorized:
            serializer.save()
            return Response({"authorized": True, "reason": None}, status=status.HTTP_200_OK)
        return Response({"authorized": False, "reason": reason}, status=status.HTTP_200_OK)
