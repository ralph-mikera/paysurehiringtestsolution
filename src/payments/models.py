from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class Payment(models.Model):
    external_user_id = models.CharField(max_length=255)
    benefit = models.CharField(max_length=255)
    currency = models.CharField(max_length=255)
    amount = models.BigIntegerField()
    timestamp = models.DateTimeField(auto_now_add=True)
